export default {
  colors: {
    turquesa: '#4ecac0',
    green: '#58c11e',
    greenDark: '#00d49d',
    yellow: '#fbffc8',
    red: '#f96737',
    gray: '#CFDFE7',
    white: '#dce5db',
    black: '#000',
    blue: '#2FA1DE'
  }
}
