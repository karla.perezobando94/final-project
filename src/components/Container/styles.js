import styled from "styled-components"

export const Contenido = styled.div`
position: relative
top: 70px
margin-top: 60px;
margin-right: 100px;
border-radius: 5px
width: 100%;
`

export const Titulo = styled.div`
background: ${(props) => props.theme.colors.white};
margin: auto;
margin-top: 30px;
padding: 10px;
text-align: justify;
position: relative
`