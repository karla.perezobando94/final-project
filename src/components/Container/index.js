import React from "react"
import { Titulo, Contenido } from "./styles"

const Container = ({titulo, children}) => {
    return (
<div>
    <Titulo>{titulo}</Titulo>
    <Contenido>{children}</Contenido>
</div>
    )
}
export default Container
