import styled from 'styled-components'

export const Wrapper = styled.form`
  background: ${(props) => props.theme.colors.white};
  box-shadow: 0px 0px 15px 15px ${(props) => props.theme.colors.gray}40;
  margin: auto;
  margin-bottom: 10px;
  border: 1px solid gray;
  padding: 10px;
  width: 800px;
  height: 300px;
  text-align: center;
  `
