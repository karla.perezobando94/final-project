import React, {useState} from "react"
import {Boton} from '../../containers/Home/styles'
import Container from "../../components/Container"
import SelectOp from "../../components/Select"

export default ({children, nextButton, index, max}) => {

    return(
        <div>
          <span>Seleccione la respuesta correcta: </span>
          <Container titulo={children.pregunta}>
          <SelectOp contenido={children.opciones} />
          </Container>
           <Boton type="submit" onClick={nextButton(index + 1)}>{max === index+1 ? "Finalizar" : "Siguiente"}</Boton>
        </div>
    )
}



          
         