import React, { useState} from "react"
import Select from "react-select"

const SelectOp = ({ contenido }) => {
  
const [selectedRespuesta, setSelectedRespuesta] = useState();

const handleChange = selectedOp => {
  const selectedRespuesta = selectedOp;
 setSelectedRespuesta({selectedRespuesta}); 

};

  return (
    <div>  
<Select onChange={handleChange} placeholder="Seleccione una.." options={contenido.map((item, index) => (
       {label: item.nombreOpcion, value: item.esVerdadera + item.nombreOpcion} 
    ))
    } />
 {selectedRespuesta ? <span style={{"color":"green"}}>Respuesta seleccionada</span>:<span style={{"color":"red"}}>Seleccione una opcion de la lista</span> }
</div>
  )}
export default SelectOp;
