import styled from 'styled-components'

export const Titulo = styled.h2`
  color: ${(props) => props.theme.colors.black};
  font-weight: bold;
  font-family: 'Arial';
`

export const Wrapper = styled.form`
  background: ${(props) => props.theme.colors.gray};
  margin: auto;
  margin-top: 50px;
  border: 2px solid gray;
  border-radius: 7px;
  padding: 10px;
  width: 280px;
  height: 300px;
  text-align: center;
`
export const Button = styled.button`
  background: ${props => props.theme.colors.gray};
  color: ${props => props.primary ? "white" : "blue"};
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 1px solid blue;
  border-radius: 3px;
`;
