import React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import { useHistory, Redirect } from "react-router-dom";
import { FormGroup, Label, Input } from "reactstrap";
import { useAuth } from "../../configuracion/useAuth";
import { Wrapper, Titulo, Button } from "./styles";
import * as yup from "yup";

export default () => {
  const history = useHistory();
  const auth = useAuth();

  if (auth.autenticado) return <Redirect to="/" />;

  const validaciones = yup.object().shape({
    username: yup.string().required("** Campo obligatorio"),
    password: yup.string().required("** Campo obligatorio")
  })

  return (
    <Wrapper>
      <Formik
      initialValues={{ username: "", password: "" }}
      onSubmit={user => {
        auth.login(user, () => history.push("/"));
      }}
      validationSchema={validaciones}
      >
       {({ values, handleChange, handleSubmit }) => (
         <Form onSubmit={handleSubmit}>
         <Titulo>Inicia Sesión</Titulo>
         <FormGroup>
           <Label htmlFor="username">Usuario:</Label>
           <br />
           <Input
             id="username"
             type="text"
             name="username"
             values={values.username}
             onChange={handleChange}
           />
           <br />
           <ErrorMessage className="Form-Error" component="span" name="username" />
         </FormGroup>
         <br />
         <FormGroup>
           <Label htmlFor="password">Contreseña:</Label>
           <br />
           <Input
             id="password"
             type="password"
             name="password"
             values={values.password}
             onChange={handleChange}
           />
           <br />
           <ErrorMessage className="Form-Error" component="span" name="password" />
         </FormGroup>
         <br />
         <Button 
         type="submit"
         >
           Entrar
         </Button>
         </Form>
       )}
      </Formik>
    </Wrapper>
  )}
