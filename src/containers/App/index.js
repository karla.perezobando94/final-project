import React from "react";
import { Switch, Route } from "react-router-dom";
import Login from "../Login";
import Home from "../Home"
import UriProtegida from "../../configuracion/UriProtegida"
import "./styles.css";

export default function App() {
  return (
    <Switch>
      <Route path="/login" component={Login} />
      <UriProtegida path="/" Component={Home} />
    </Switch>
  );
}
