import React, {useState} from "react"
import { useHistory } from "react-router-dom"
import { Formik, Form } from "formik";
import { Wrapper } from "../Home/styles"
import { useAuth } from "../../configuracion/useAuth"
import { encuesta } from "../../encuesta"
import { WrapperContainer, Boton } from "./styles"
import Validacion from "../../components/Validacion"

export default () => {
  const auth = useAuth();
  const history = useHistory();

  const [selectedIndex, setSelectedIndex] = useState(0);

  const handleSubmit= (index) => (event) => {
    console.warn(index)
    setSelectedIndex(index)
    event.preventDefault();
  };

    return(
      <div>
      <Wrapper>
        <h3>¡Bienvenid@!</h3>  
        <button
          onClick={() => {
          auth.logout(() => history.push("/login"));
          }}
        >
        Cerrar Sesion
        </button>
        </Wrapper>
      <Formik>
      <Form>
        { encuesta.map((opciones, index) => {
        return ( selectedIndex === index && 
          <WrapperContainer>
            <Validacion index={index} max ={encuesta.length} nextButton={handleSubmit}>{opciones}</Validacion>
          </WrapperContainer>
        )
      })}
        </Form>
    </Formik>
    </div>
    )
}

/*

*/