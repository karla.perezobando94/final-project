import styled from 'styled-components'

export const WrapperContainer = styled.div`
  background: ${(props) => props.theme.colors.white};
  box-shadow: 0px 0px 15px 15px ${(props) => props.theme.colors.gray}40;
  margin: auto;
  margin-bottom: 10px;
  border: 1px solid gray;
  padding: 10px;
  width: 800px;
  height: 300px;
  text-align: center;
  `
  export const Wrapper = styled.div`
  background: ${(props) => props.theme.colors.blue};
  margin-bottom: 10px;
  border: 1px solid gray;
  padding: 10px;
  height: 30px;
  position: relative;
  display: flex;
  justify-content: space-between;
  border-radius: 5px;
  `
  export const Boton = styled.button`
  background: ${(props) => props.theme.colors.green};
  margin-top: 10px;  
  padding: 10px;
  width: 130px;
  height: 35px;
  `
