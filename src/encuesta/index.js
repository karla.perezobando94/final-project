export const encuesta = [
    {
      "pregunta": "¿Cuál es el resultado de sumar 2 + 2?",
      "opciones": [
          {
           "nombreOpcion": "a. 6",
           "esVerdadera": false
          },
          {
           "nombreOpcion": "b. 3",
           "esVerdadera": false
          },
          {
            "nombreOpcion": "c. 4",
            "esVerdadera": true 
          }
         ]
        },
        {
        "pregunta": "¿Cuál es el resultado de multiplicar 2 * 2?",
        "opciones": [
          {
           "nombreOpcion": "a. 4",
           "esVerdadera": true
           },
        
          {
           "nombreOpcion": "b. 3",
           "esVerdadera": false
          },
          {
            "nombreOpcion": "c. 1",
            "esVerdadera": false
          } 
        ]
        },
        {
        "pregunta": "¿Cuál es el resultado de dividir 4 / 2?",
        "opciones": [
          {
           "nombreOpcion": "a. 8",
           "esVerdadera": false
           },
        
          {
           "nombreOpcion": "b. 2",
           "esVerdadera": true
          },
          {
           "nombreOpcion": "a. 6",
           "esVerdadera": false
          } 
        ]
        },
        {
        "pregunta": "¿Cuál es el resultado de sumar 10 + 11?",
        "opciones": [
          {
           "nombreOpcion": "a. 21",
           "esVerdadera": true
           },
        
          {
           "nombreOpcion": "b. 17",
           "esVerdadera": false
          },
          {
           "nombreOpcion": "c. 22",
           "esVerdadera": false
          } 
        ]
        },
        {
        "pregunta": "¿Cuál es el resultado de restar 40 - 4?",
        "opciones": [
          {
            "nombreOpcion": "a. 46",
            "esVerdadera": false
           },
        
          {
           "nombreOpcion": "b. 54",
           "esVerdadera": false
          },
          {
           "nombreOpcion": "c. 36",
           "esVerdadera": true
          } 
        ]
    }]
    